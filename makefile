
#
# Exemple de fichier Makefile pour lancer le formattage
# du fichier article.tex

all: papier.pdf

papier.pdf papier.latex: papier.md template.tex prelude.tex
	pandoc papier.md --filter pandoc-citeproc --template template.tex -o $@
	# pandoc papier.md --bibliography=article.bib --template template.tex -o $@

papier.html :; pandoc -o $@ papier.md

jres-2017.pdf jres-2017.latex: jres-2017.md template.tex prelude.tex
	pandoc jres-2017.md --filter pandoc-citeproc --template template.tex -o $@
	#pandoc jres-2017.md --cls=./jres.cls --filter pandoc-citeproc --template template.tex -o $@
	#pandoc jres-2017.md --bibliography=article.bib --template template.tex -o $@


CIBLES = article.pdf
PDFLATEX = pdflatex

all:	$(CIBLES)

# 3 passages de latex pour assurer une coherence de la
# bibliographie
article.pdf: article.tex article.bib
	$(PDFLATEX) article.tex
	bibtex article
	$(PDFLATEX) article.tex
	$(PDFLATEX) article.tex

clean:
	rm -f $(CIBLES) *.aux *.log *.blg *.bbl *.out
